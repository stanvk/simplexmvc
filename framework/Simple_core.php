<?php

define('LIB', '/libraries');

require LIB . '/Controller.php';
require LIB . '/Model.php';
require LIB . '/View.php';
require LIB . '/Input.php';
//require LIB . '/Libraries.php';
require LIB . '/Bootstrap.php';

//Require the router.
require './Routes.php'; //Quite important stuff..