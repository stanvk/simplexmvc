<?php

switch ( REPORT_ERRORS )
{

    case 'developing':
        ini_set('error_reporting', true);
        break;
    
    case 'running':
        ini_set('error_reporting', false);
        break;
}


ini_set('session.cookie_httponly', COOKIE_SECURITY);

ob_start();
session_start();

require SYS . 'Simple_core.php';

try
{
    new Bootstrap( $ROUTE );
}
catch ( Exception $e )
{
    echo '<h2>Simplex System Error</h2><hr><strong>Message:</strong> <em> "' . $e->getMessage() . '"</em><br><hr>';
    echo '<br><strong>SYS</strong><hr><strong>File:</strong> <em> "' . $e->getFile() . '"</em><br>';
    echo '<strong>Line:</strong> <em> "' . $e->getLine() . '"</em><br>';
}
