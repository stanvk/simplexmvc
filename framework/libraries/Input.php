<?php

class Input
{

    private $_routes;
    private $_controller, $_params;
    private $_url;

    public function __construct ( $routes )
    {
        $url = $_SERVER['REQUEST_URI'];

        $INDEX_PATH = str_replace(' ', '%20', INDEX_PATH);
        $url = str_replace($INDEX_PATH, '', $url);
        $url = explode('/', ltrim($url, '/'));

        $this->_url = $url;

        $this->_controller = $url[0];

        unset($url[0]);
        $this->_params = $url;
        
        $this->_routes = $routes;
    }

    public function get($key, $xss = false)
    {
        $params = $this->_params;
        $controller = $this->_controller;
        $routes = $this->_routes;

        $url = $this->_url;
		
        foreach ( $routes as $route => $route_controllers )
        {
            $route_elements[] = explode('/', $route);
        }

        foreach ( $route_elements as $route_param => $param )
        {
		
            if ( in_array($key, $param) )
            {
                if ( $xss === TRUE )
                {
                    return htmlentities($url[array_search($key, $param)]);
                }
                else
                {
                    return $url[array_search($key, $param)];
                }
            }
			
        }

    }

    public function post ( $key, $xss = false )
    {
        if ( $xss )
        {
            $post = htmlentities($_POST[$key]);
        }
        else
        {
            $post = $_POST[$key];
        }

        return $post;
    }

    public function session ( $key, $value = false )
    {

        if ( $value )
        {
            $_SESSION[$key] = $value;
        }

        if ( isset($_SESSION[$key]) )
        {
            return $_SESSION[$key];
        }
        else
        {
            return 'No session called: "' . $key . '" found!';
        }
    }

    public function cookie ( $key )
    {

        if ( isset($_COOKIE[$key]) )
        {
            return $_COOKIE[$key];
        }
        else
        {
            return 'No cookie called: "' . $key . '" found!';
        }
    }

    public function server ( $key )
    {
        if ( isset($_SERVER[$key]) )
        {
            return $_SERVER[$key];
        }
        else
        {
            return 'No server variable called: "' . $key . '" found!';
        }
    }

}
