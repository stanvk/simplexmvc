<?php

class Model
{
    
    //test
    public $database;
    
    public static function init ()
    {
        $this->database = new Database();
    }
    
    public function load ( $model )
    {
        if ( file_exists(APP . 'models/' . $model . '_model.php') )
        {
            require APP . 'models/' . $model . '_model.php';
            $this->{$model} = new $model();
        }
        else
        {
            throw new Exception("No model found");
        }
    }
    
    //protected function loadDatabase () {}
    
}