<?php

class Bootstrap
{
    
    //private $_url;
    //private $_page, $_action;
    private $_params, $_controller;
    private $_routes;
    
    public static $routes;
    
    public function __construct ( Array $routes )
    {
    
        $this->_routes = $routes;
        self::$routes = $routes;

        $url = $_SERVER['REQUEST_URI'];
        $url = $this->decomposeUrl($url);
        
        $this->_params = $url['params'];
        
        if ( $url['page'] )
        {
            $this->loadController( $url['page'] );
        }
        else
        {
            $this->loadController( 'index' );
        }
        
    }
    
    private function loadController ( $page )
    {
        
        if ( $this->checkRouteExists($page) && file_exists(APP . 'controllers/' . $page . '_controller.php') )
        {
            $controller = str_replace('_controller.php', '', $page);
            
            require APP . 'controllers/' . $page . '_controller.php';
            $this->_controller = new $controller( $this->_params );
        }
        else
        {
            if ( isset($this->_routes['error']) && file_exists(APP . 'controllers/' . $this->_routes['error']) )
            {
                require APP . 'controllers/' . $this->_routes['error'];
                $this->_controller = new error( $this->_params );
            }
            else
            {
                throw new Exception("No route was given to the error class!");
            }
        }
    }

    private function decomposeUrl ( $url )
    {
        $INDEX_PATH = str_replace(' ', '%20', INDEX_PATH);
        $url = str_replace($INDEX_PATH, '', $url);
        $url = explode('/', ltrim($url, '/'));
        
        @$page = $url[0];
        
        unset($url[0]);
        $params = $url;
        
        $url = array(
            'page' => $page,
            'params' => $params
        );
       
        return $url;
    }
    
    private function checkRouteExists ( $page )
    {
        $routes = $this->_routes;
        
        foreach ( $routes as $route => $route_controllers )
        {
            $routes_array[] = explode('/', $route);
        }
        
        foreach ( $routes_array as $route => $params )
        {
            if ( in_array($page, $params) )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    
}