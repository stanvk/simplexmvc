<?php

class View
{
    
    public function load ( $view, $data = false )
    {
        if ( file_exists(APP . 'views/' . $view) )
        {
            if ( @is_array($data) )
            {
                @extract($data);
            }
            
            require APP . 'views/' . $view;
        }
        else
        {
            throw new Exception("View wasn't found!");
        }
    }
    
}