<?php

/*
 * =============================================================================
 * Copyright (c) 2013 Stanvk
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * =============================================================================
 *
 * Define the path from the webservers root folder to this file.
 * For example: 'test/source/test'
 *
 * Leave it empty if the framework is in the root folder of your webserver.
 */
define('INDEX_PATH', '');

/*
 * Define the status of your project built in this framework.
 * For example: 'developing'
 *
 * @Params: 'developing', 'running'
 */
define('REPORT_ERRORS', 'developing');

/*
 * Define the bool value for your cookie security.
 * Basically, it will just turn-on the http_only ini setting.
 * 
 * @Params: true, false
 */
define('COOKIE_SECURITY', true);

/*
 * Define the path to the application folder.
 */
define('APP', 'application/');

/*
 * Define the path to the framework folder.
 */
define('SYS', 'framework/');

/*
 * Catch the boot file and run the whole framework.
 */
require SYS . 'Boot.php';   