<?php

//index, error; these controllers are build-in and have to be defined! 

$ROUTE['index/:id'] = 'index_controller.php'; //The :id parameter is a get that will be returned in the index_controller.php
$ROUTE['error'] = 'error_controller.php';
$ROUTE['test/:zooi'] = 'test_controller.php';